<?php

use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\OutletController;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\PaymentMethodController;
use App\Http\Controllers\TreatmentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api', 'cors', 'json.response']], function () {
    Route::apiResource('owners', OwnerController::class);
    Route::apiResource('outlets', OutletController::class);
    Route::apiResource('payment-methods', PaymentMethodController::class);
    Route::apiResource('treatments', TreatmentController::class);

    Route::post('process-cart', [InvoiceController::class, 'processCart']);
});
