FROM php:8.1.9-fpm
RUN apt-get update -y && apt-get install -y libmcrypt-dev openssl git
RUN pecl install mcrypt-1.0.5
RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install mysqli pdo pdo_mysql
WORKDIR /app
COPY . /app
RUN composer install

CMD php artisan serve --host=0.0.0.0 --port=8000
EXPOSE 8000
