<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TreatmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:5|max:100',
            'description' => 'required|string|max:100',
            'price' => 'required|numeric|min:0|max:9999999',
            'outlet_id' => 'required|exists:outlets,id',
            'image' => 'mimes:png,jpg|max:2048',
        ];
    }
}
