<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OutletStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'owner_id' => 'required|exists:owners,id',
            'address' => 'required|string|min:10|max:200',
            'image' => 'mimes:png,jpg|max:2048',
        ];
    }
}
