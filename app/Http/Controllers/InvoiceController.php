<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProcessCartRequest;
use App\Http\Resources\InvoiceResource;
use App\Models\Invoice;
use App\Services\InvoiceService;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    private InvoiceService $invoiceService;

    public function __construct(InvoiceService $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function processCart(ProcessCartRequest $request)
    {
        $user = auth()->user();

        $invoice = $this->invoiceService->processCart($user->id, $request->payment_method_id, $request->treatment_ids);

        return new InvoiceResource($invoice);
    }
}
