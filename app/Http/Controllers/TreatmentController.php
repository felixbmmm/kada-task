<?php

namespace App\Http\Controllers;

use App\Http\Requests\TreatmentStoreRequest;
use App\Http\Requests\TreatmentUpdateRequest;
use App\Http\Resources\TreatmentResource;
use App\Models\Treatment;
use App\Services\TreatmentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TreatmentController extends Controller
{
    private TreatmentService $treatmentService;

    public function __construct(TreatmentService $treatmentService)
    {
        $this->treatmentService = $treatmentService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $treatments = $this->treatmentService->getAll();

        return TreatmentResource::collection($treatments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TreatmentStoreRequest $request)
    {
        $imageName = "";
        if ($request->file('image')) {
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = str_replace(' ', '_', $imageName);
            $request->file('image')->storeAs('images/treatments/', $imageName);
        }
        $treatment = $this->treatmentService->createTreatment($request->name, $request->description, $request->price, $request->outlet_id, $imageName);

        return new TreatmentResource($treatment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Treatment  $treatment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $treatment = $this->treatmentService->getById($id);

        return new TreatmentResource($treatment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Treatment  $treatment
     * @return \Illuminate\Http\Response
     */
    public function update(TreatmentUpdateRequest $request, $id)
    {
        $treatment = $this->treatmentService->getById($id);
        $imageName = "";
        if ($request->file('image')) {
            Storage::delete('public/treatments/' . $treatment->image);
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = str_replace(' ', '_', $imageName);
            $request->file('image')->storeAs('public/treatments/', $imageName);
        }
        $treatment = $this->treatmentService->updateById($id, $request->name, $request->description, $request->price, $request->outlet_id, $imageName);

        return new TreatmentResource($treatment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Treatment  $treatment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->treatmentService->deleteById($id);

        if ($result) return response()->json([], 204);
    }
}
