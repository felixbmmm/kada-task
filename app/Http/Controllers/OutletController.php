<?php

namespace App\Http\Controllers;

use App\Http\Requests\OutletStoreRequest;
use App\Http\Requests\OutletUpdateRequest;
use App\Http\Resources\OutletResource;
use App\Models\Outlet;
use App\Services\OutletService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OutletController extends Controller
{
    private OutletService $outletService;

    public function __construct(OutletService $outletService)
    {
        $this->outletService = $outletService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $outlets = $this->outletService->getAll();

        return OutletResource::collection($outlets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OutletStoreRequest $request)
    {
        $imageName = "";
        if ($request->file('image')) {
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = str_replace(' ', '_', $imageName);
            $request->file('image')->storeAs('images/outlets/', $imageName);
        }
        $outlet = $this->outletService->createOutlet($request->owner_id, $request->address, $imageName);

        return new OutletResource($outlet);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $outlet = $this->outletService->getById($id);

        return new OutletResource($outlet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function update(OutletUpdateRequest $request, $id)
    {
        $outlet = $this->outletService->getById($id);
        $imageName = "";
        if ($request->file('image')) {
            Storage::delete('public/outlets/' . $outlet->image);
            $imageName = $request->file('image')->getClientOriginalName();
            $imageName = str_replace(' ', '_', $imageName);
            $request->file('image')->storeAs('public/outlets/', $imageName);
        }
        $outlet = $this->outletService->updateById($id, $request->owner_id, $request->address, $imageName);

        return new OutletResource($outlet);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Outlet  $outlet
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->outletService->deleteById($id);

        if ($result) return response()->json([], 204);
    }
}
