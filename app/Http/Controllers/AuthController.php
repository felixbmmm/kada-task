<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Login
     *
     * @param \App\Http\Requests\LoginRequest
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) return response()->json(['message' => 'User with that email does not exist.'], 401);

        if (Auth::guard('apiClient')->attempt(['email' => $user->email, 'password' => $request->password])) {
            $token =  $user->createToken('login-secret')->accessToken;
            return response()->json(['user' => $user, 'token' => $token], 200);
        }
        else {
            return response()->json(['message' => 'Email or password does not match.'], 401);
        }
    }

    /**
     * Logout
     * 
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) 
    {
        $token = $request->user()->token();
        $token->revoke();

        return response([], 204);
    }

    public function register(RegisterRequest $request) 
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'date_of_birth' => $request->date_of_birth,
        ]);

        $token =  $user->createToken('login-secret')->accessToken;
        return response()->json(['user' => $user, 'token' => $token], 201);
    }
}
