<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'payment_method_id' => $this->payment_method_id,
            'invoiced_at' => $this->invoiced_at,
            'total_price' => $this->getTotalPriceAttribute(),

            'user' => new UserResource($this->user),
            'payment_method' => new PaymentMethodResource($this->paymentMethod),
            'invoice_items' => InvoiceItemResource::collection($this->invoiceItems),
        ];
    }
}
