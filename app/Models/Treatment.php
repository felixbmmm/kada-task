<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'price', 'outlet_id', 'image'];

    protected $table = 'treatments';

    public function outlet()
    {
        return $this->belongsTo(Outlet::class);
    }
}
