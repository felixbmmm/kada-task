<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    use HasFactory;

    protected $fillable = ['owner_id', 'address', 'image'];

    protected $table = 'outlets';

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }
}
