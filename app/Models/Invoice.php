<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'payment_method_id', 'invoiced_at'];

    protected $table = 'invoices';

    protected $casts = [
        'invoiced_at' => 'date',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function invoiceItems()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function getTotalPriceAttribute()
    {
        $totalPrice = 0;
        foreach ($this->invoiceItems as $invoiceItem)
        {
            $totalPrice += $invoiceItem->price;
        }

        return $totalPrice;
    }
}
