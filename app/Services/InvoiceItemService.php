<?php

namespace App\Services;

use App\Models\InvoiceItem;

class InvoiceItemService
{
    private TreatmentService $treatmentService;

    public function __construct(TreatmentService $treatmentService)
    {
        $this->treatmentService = $treatmentService;
    }

    public function createInvoiceItem(string $invoiceId, string $treatmentId)
    {
        $treatment = $this->treatmentService->getById($treatmentId);

        InvoiceItem::create([
            'invoice_id' => $invoiceId,
            'treatment_id' => $treatmentId,
            'price' => $treatment->price,
        ]);
    }
}
