<?php

namespace App\Services;

use App\Http\Resources\OwnerResource;
use App\Models\Owner;
use Spatie\QueryBuilder\QueryBuilder;

class OwnerService
{
    public function getAll()
    {
        $owners = QueryBuilder::for(Owner::class)
            ->allowedFilters([
                'name',
            ])
            ->get();

        return $owners;
    }

    public function createOwner(string $name)
    {
        $owner = Owner::create([
            'name' => $name,
        ]);

        return $owner;
    }

    public function getById($id)
    {
        $owner = Owner::findOrFail($id);

        return $owner;
    }

    public function updateById($id, string $name)
    {
        $owner = Owner::where('id', $id)->update([
            'name' => $name,
        ]);

        return Owner::findOrFail($id);
    }

    public function deleteById($id)
    {
        Owner::where('id', $id)->delete();

        return true;
    }
}