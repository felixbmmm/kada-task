<?php

namespace App\Services;

use App\Models\Treatment;
use Spatie\QueryBuilder\QueryBuilder;

class TreatmentService
{
    public function getAll()
    {
        $treatments = QueryBuilder::for(Treatment::class)
            ->allowedFilters([
                'outlet_id',
            ])
            ->get();

        return $treatments;
    }

    public function createTreatment(string $name, string $description, string $price, string $outletId, string $image)
    {
        $treatment = Treatment::create([
            'name' => $name,
            'description' => $description,
            'price' => $price,
            'outlet_id' => $outletId,
            'image' => $image,
        ]);

        return $treatment;
    }

    public function getById($id)
    {
        $treatment = Treatment::findOrFail($id);

        return $treatment;
    }

    public function updateById($id, string $name, string $description, string $price, string $outletId, string $image)
    {
        $treatment = Treatment::where('id', $id)->update([
            'name' => $name,
            'description' => $description,
            'price' => $price,
            'outlet_id' => $outletId,
            'image' => $image,
        ]);

        return Treatment::findOrFail($id);
    }

    public function deleteById($id)
    {
        Treatment::where('id', $id)->delete();

        return true;
    }
}