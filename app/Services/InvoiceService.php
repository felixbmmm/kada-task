<?php

namespace App\Services;

use App\Models\Invoice;
use Carbon\Carbon;

class InvoiceService
{
    private InvoiceItemService $invoiceItemService;

    public function __construct(InvoiceItemService $invoiceItemService)
    {
        $this->invoiceItemService = $invoiceItemService;
    }

    public function processCart(string $userId, string $paymentMethodId, array $treatmentIds)
    {
        $invoice = Invoice::create([
            'user_id' => $userId,
            'payment_method_id' => $paymentMethodId,
            'invoiced_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        foreach ($treatmentIds as $treatmentId)
        {
            $this->invoiceItemService->createInvoiceItem($invoice->id, $treatmentId);
        }

        return $invoice;
    }
}
