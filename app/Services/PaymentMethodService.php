<?php

namespace App\Services;

use App\Models\PaymentMethod;
use Spatie\QueryBuilder\QueryBuilder;

class PaymentMethodService
{
    public function getAll()
    {
        $paymentMethods = QueryBuilder::for(PaymentMethod::class)
            ->allowedFilters([
                'header',
            ])
            ->get();

        return $paymentMethods;
    }

    public function createPaymentMethod(string $header, string $name)
    {
        $paymentMethod = PaymentMethod::create([
            'header' => $header,
            'name' => $name,
        ]);

        return $paymentMethod;
    }

    public function getById($id)
    {
        $paymentMethod = PaymentMethod::findOrFail($id);

        return $paymentMethod;
    }

    public function updateById($id, string $header, string $name)
    {
        $paymentMethod = PaymentMethod::where('id', $id)->update([
            'header' => $header,
            'name' => $name,
        ]);

        return PaymentMethod::findOrFail($id);
    }

    public function deleteById($id)
    {
        PaymentMethod::where('id', $id)->delete();

        return true;
    }
}