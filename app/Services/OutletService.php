<?php

namespace App\Services;

use App\Models\Outlet;
use Spatie\QueryBuilder\QueryBuilder;

class OutletService
{
    public function getAll()
    {
        $outlets = QueryBuilder::for(Outlet::class)
            ->allowedFilters([
                'owner_id',
            ])
            ->get();

        return $outlets;
    }

    public function createOutlet(string $ownerId, string $address, string $image)
    {
        $outlet = Outlet::create([
            'owner_id' => $ownerId,
            'address' => $address,
            'image' => $image,
        ]);

        return $outlet;
    }

    public function getById($id)
    {
        $outlet = Outlet::findOrFail($id);

        return $outlet;
    }

    public function updateById($id, string $ownerId, string $address, string $image)
    {
        $outlet = Outlet::where('id', $id)->update([
            'owner_id' => $ownerId,
            'address' => $address,
            'image' => $image,
        ]);

        return Outlet::findOrFail($id);
    }

    public function deleteById($id)
    {
        Outlet::where('id', $id)->delete();

        return true;
    }
}